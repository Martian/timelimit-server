# SendMailLoginCodeRequest Schema

```txt
https://timelimit.io/SendMailLoginCodeRequest
```




| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                          |
| :------------------ | ---------- | -------------- | ------------ | :---------------- | --------------------- | ------------------- | --------------------------------------------------------------------------------------------------- |
| Can be instantiated | Yes        | Unknown status | No           | Forbidden         | Forbidden             | none                | [SendMailLoginCodeRequest.schema.json](SendMailLoginCodeRequest.schema.json "open original schema") |

## SendMailLoginCodeRequest Type

`object` ([SendMailLoginCodeRequest](sendmaillogincoderequest.md))

# SendMailLoginCodeRequest Definitions

# SendMailLoginCodeRequest Properties

| Property          | Type     | Required | Nullable       | Defined by                                                                                                                                        |
| :---------------- | -------- | -------- | -------------- | :------------------------------------------------------------------------------------------------------------------------------------------------ |
| [mail](#mail)     | `string` | Required | cannot be null | [SendMailLoginCodeRequest](sendmaillogincoderequest-properties-mail.md "https&#x3A;//timelimit.io/SendMailLoginCodeRequest#/properties/mail")     |
| [locale](#locale) | `string` | Required | cannot be null | [SendMailLoginCodeRequest](sendmaillogincoderequest-properties-locale.md "https&#x3A;//timelimit.io/SendMailLoginCodeRequest#/properties/locale") |

## mail




`mail`

-   is required
-   Type: `string`
-   cannot be null
-   defined in: [SendMailLoginCodeRequest](sendmaillogincoderequest-properties-mail.md "https&#x3A;//timelimit.io/SendMailLoginCodeRequest#/properties/mail")

### mail Type

`string`

## locale




`locale`

-   is required
-   Type: `string`
-   cannot be null
-   defined in: [SendMailLoginCodeRequest](sendmaillogincoderequest-properties-locale.md "https&#x3A;//timelimit.io/SendMailLoginCodeRequest#/properties/locale")

### locale Type

`string`
