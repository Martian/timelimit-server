# RequestWithAuthToken Schema

```txt
https://timelimit.io/RequestWithAuthToken
```




| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                  |
| :------------------ | ---------- | -------------- | ------------ | :---------------- | --------------------- | ------------------- | ------------------------------------------------------------------------------------------- |
| Can be instantiated | Yes        | Unknown status | No           | Forbidden         | Forbidden             | none                | [RequestWithAuthToken.schema.json](RequestWithAuthToken.schema.json "open original schema") |

## RequestWithAuthToken Type

`object` ([RequestWithAuthToken](requestwithauthtoken.md))

# RequestWithAuthToken Definitions

# RequestWithAuthToken Properties

| Property                            | Type     | Required | Nullable       | Defined by                                                                                                                                              |
| :---------------------------------- | -------- | -------- | -------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [deviceAuthToken](#deviceAuthToken) | `string` | Required | cannot be null | [RequestWithAuthToken](requestwithauthtoken-properties-deviceauthtoken.md "https&#x3A;//timelimit.io/RequestWithAuthToken#/properties/deviceAuthToken") |

## deviceAuthToken




`deviceAuthToken`

-   is required
-   Type: `string`
-   cannot be null
-   defined in: [RequestWithAuthToken](requestwithauthtoken-properties-deviceauthtoken.md "https&#x3A;//timelimit.io/RequestWithAuthToken#/properties/deviceAuthToken")

### deviceAuthToken Type

`string`
